var  fs = require('fs')
    , db = require('mongojs').connect('HomeControl');

var collections = ['rooms', 'devices', 'settings', 'widgets', 'widget_device', 'scenarios'];

collections.forEach(function (table) {
    var collection = db.collection(table);
    collection.remove();
    var rows = require(__dirname+'/data/'+table+'.json');
    rows.forEach(function (row) {
        collection.insert(row, function (err, doc) {
            if (err) {throw err;}
        });
    });
    console.log(table+' load to MongoDB');
});
