var socket = {};
var route = {};
var routing = function() {

    route = window.location.hash.split('#')[1];
    route = route ? route.split('/') : '';
    for (var i = 0; i < route.length; i++) {
        if (!route) {
            route.splice(i, 1);
        }
    }
    var controller = route[0] ? route[0] : 'rooms';
    Controllers[controller].run();
}

$(function() {
    host = window.location.hostname;
    $.getScript('http://' + host + ':8001/socket.io/socket.io.js', function(){
        socket = io.connect('http://' + host + ':8001');
        socket.on('connect', function () {
            window.reload = function () {
                socket.emit('disconnect');
            };
            $(window).bind('hashchange', routing);
            routing();

        });
    });
});

var intervals = {};
var addEventInterval = function(id, func, interval) {
    if (intervals[id]) {
        clearInterval(intervals[id]);
    }
    intervals[id] = setInterval(func, interval);
}

var Controllers = {
    settings: {
        params: {},

        run: function() {
            var _this = this;
            $('#mainbar ul li.settings').addClass('active').siblings('li').removeClass('active');
            this.params.action = route[1] ? route[1] : false;
            this.params.setting = this.params.action;

            var settings = {
                'devices': 'Devices',
                'widgets': 'Widgets',
                'rooms': 'Rooms',
                'scenarios': 'Scenarios',
                'system': 'System',
            }

            $('#sidebar').html(new EJS({url: '/templates/settings.ejs'}).render({settings: settings, setting_active: _this.params.setting}));

            if (this.params.action) {
                this[this.params.action]();
            }
        },

        index: function() {
            $('#content').html(new EJS({url: '/templates/settings/index.ejs'}).render({}));
        },

        devices: function() {
            socket.emit('get_all_devices', {}, function(data) {
                $('#content').html(new EJS({url: '/templates/settings/devices.ejs'}).render({'devices': data}));
            });
        },

        widgets: function() {
            socket.emit('get_all_widgets', {}, function(data) {
                $('#content').html(new EJS({url: '/templates/settings/widgets.ejs'}).render({'widgets': data}));
            });
        },

        rooms: function() {
            socket.emit('get_all_rooms', {}, function(data) {
                $('#content').html(new EJS({url: '/templates/settings/rooms.ejs'}).render({'rooms': data}));
            });
        },

        add_room: function(data) {
            if (data) {
                socket.emit('get_all_rooms', {}, function(data) {
                    window.location.hash = '#settings/rooms';
                });
            } else {
                    $('#content').html(new EJS({url: '/templates/settings/add_room.ejs'}).render({}));
            }
        },

        scenarios: function() {
            socket.emit('get_all_scenarios', {}, function(data) {
                $('#content').html(new EJS({url: '/templates/settings/scenarios.ejs'}).render({'scenarios': data}));
            });
        },

        system: function() {
            $('#content').html(new EJS({url: '/templates/settings/system.ejs'}).render({}));
        },
    },

    rooms: {
        params: {},

        run: function() {
            var _this = this;
            $('#mainbar ul li.home').addClass('active').siblings('li').removeClass('active');
            this.params.room = route[1] ? route[1] : false;
            this.params.action = route[1] && route[2] && this[route[2]] ? route[2] : 'index';

            socket.emit('get_all_rooms', {}, function(data) {
                $('#sidebar').html(new EJS({url: '/templates/rooms.ejs'}).render({'rooms': data, room_active: _this.params.room}));
                $('#rooms li').click(function() {
                    $(this).siblings().removeClass('active');
                    $(this).addClass('active');
                });
            });

            this[this.params.action]();
        },

        index: function() {
            var content = [];

            socket.emit('get_dashboard_widgets', {}, function(widgets) {
                widgets.forEach(function (widget) {
                    socket.emit('get_widget_device', {id: widget.id}, function(data) {
                        if (data.widget) {
                            content[data.id] = new EJS({url: '/templates/widgets/' + data.widget.name + '/' + data.widget.name + '.ejs'}).render({'data': data});
                        }
                        $('#content').html(content.join(''));
                    });
                });
            });
        },

        details: function() {
            var _this = this;
            var content = [];

            socket.emit('get_room_data', {id: _this.params.room}, function(data) {
                socket.emit('get_room_widgets', {id: _this.params.room}, function(widgets) {
                    widgets.forEach(function (widget) {
                        socket.emit('get_widget_device', {id: widget.id}, function(data) {
                            if (data.widget) {
                                content[data.id] = new EJS({url: '/templates/widgets/' + data.widget.name + '/' + data.widget.name + '.ejs'}).render({'data': data});
                            }
                            $('#content').html(content.join(''));
                        });
                    });
                });
            });

            $('#content').html(content.join(''));
        },
    },
}
