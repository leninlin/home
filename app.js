var express = require('express'),
    app = module.exports.app = express(), 
    db = require('mongojs').connect('HomeControl'),
    io = require('socket.io').listen(8001, function() {
            console.log("Socket listening on 8001");
        });

io.set('log level', 1);    
    
app.configure(function () {
    app.use(express.favicon());
    app.use(express.bodyParser());
    app.use(express.logger('dev'));  //tiny, short, default
    app.use(app.router);
    app.use(express.static(__dirname + '/app'));
    app.use(express.errorHandler({dumpExceptions: true, showStack: true, showMessage: true}));
});
app.listen(8080, function() {
    console.log("Server listening on 8080");
});

app.get('/', function (req, res) {
  res.sendfile(__dirname + '/app/index.html');
});

// Навешиваем обработчик на подключение нового клиента
io.sockets.on('connection', function (socket) {
    var events = new Events(socket);
    Object.keys(events).forEach(function(event) {
        socket.on(event, function(data, callback) {
            console.log(event, data);
            events[event](data, callback);
        });
    });
    socket.on('disconnect', function () {});
});

var Events = function (socket) {
    var socket = socket;
    var busy = {};

    return {
        remove_addresses: function(data, callback) {
            var _this = this;

            data.addresses.forEach(function(address) {
                db.collection('devices').find({address: parseInt(address)}, function(devices) {
                    if (devices) {
                        devices.forEach(function(device) {
                            socket.emit('remove_device', device);
                            socket.broadcast.emit('remove_device', device);
                            _this.on_remove_device(device);
                        });

                        db.collection('devices').remove({address: parseInt(address)});
                    }
                });
            });
            if (typeof callback == 'function') {
                callback();
            }
        },

        add_address: function(data, callback) {
            var _this = this;

            for (i in data.devices) {
                _this.add_device(data);
            }

            if (typeof callback == 'function') {
                callback();
            }
        },

        add_device: function(data, callback) {
            var _this = this;

            var newDevice = {
                id: parseInt(data.address) * 100 + parseInt(i),
                address: parseInt(data.address),
                device_num: parseInt(i),
                name: data.devices[i].name,
                sensor: data.devices[i].sensor,
                action: data.devices[i].action,
                state: parseInt(data.devices[i].state),
            };

            db.collection('devices').findOne({id: newDevice.id}, function(err, doc) {
                console.log(doc);
                if (!doc) {
                    db.collection('devices').insert(newDevice, function() {
                        if (typeof callback == 'function') {
                            callback();
                        }
                    });

                    socket.emit('add_device_data', newDevice);
                    socket.broadcast.emit('add_device_data', newDevice);
                    _this.on_add_device_data(newDevice);
                }
            });
        },

        set_device_raw_state: function (data, callback) {
            var _this = this;

            if (data.address && data.device_num) {
                db.collection('devices').findOne({address: parseInt(data.address), device_num: parseInt(data.device_num)}, function (err, doc) {
                    if (doc) {
                        if (parseInt(data.state) != doc.state) {
                            db.collection('devices').update({id: doc.id}, {$set: {state: parseInt(data.state)}}, function (err, updated) {
                                doc.state = parseInt(data.state);

                                socket.emit('change_device_data', doc);
                                socket.broadcast.emit('change_device_data', doc);
                                _this.on_change_device_data(doc);
                            });
                        }
                    }
                });
            }
        },

        get_all_rooms: function (data, callback) {
            db.collection('rooms').find({}, function(err, doc) {
                if (typeof callback == 'function') {
                    callback(doc);
                }
            });
        },

        get_room_data: function (data, callback) {
            if (data.id) {
                db.collection('rooms').findOne({id: data.id}, function(err, room) {
                    if (typeof callback == 'function') {
                        callback(room);
                    }
                });
            } else {
                if (typeof callback == 'function') {
                    callback(false);
                }
            }
        },

        remove_room: function (data, callback) {
            if (data.id) {
                db.collection('rooms').remove({id: data.id}, function(err, room) {
                    if (typeof callback == 'function') {
                        callback(room);
                    }

                    socket.broadcast.emit('removed_room', room);
                });
            } else {
                if (typeof callback == 'function') {
                    callback(false);
                }
            }
        },

        add_room: function (data, callback) {
            if (data.name) {
                db.collection('rooms').find({}, function(err, doc) {
                    var id = data.name.replace(/\s/g, '_').replace(/[,.'"`]/g, '').toLowerCase();
                    db.collection('rooms').insert({id: id, name: data.name}, function(err, new_room) {
                        db.collection('rooms').find({id: id}, function(err, room) {
                            if (typeof callback == 'function') {
                                callback(room);
                            }

                            socket.broadcast.emit('new_room', room);
                        });
                    });
                });
            } else {
                if (typeof callback == 'function') {
                    callback(false);
                }
            }
        },

        update_room: function (data, callback) {
            if (data.id) {
                db.collection('rooms').update({id: data.id}, {'$set': {name: data.name}}, function(err, room) {
                    db.collection('rooms').find({id: data.id}, function(err, room) {
                        if (typeof callback == 'function') {
                            callback(room);
                        }

                        socket.broadcast.emit('update_room', room);
                    });
                });
            } else {
                if (typeof callback == 'function') {
                    callback(false);
                }
            }
        },

        get_all_devices: function (data, callback) {
            db.collection('devices').find({}, function (err, devices) {
                if (typeof callback == 'function') {
                    callback(devices);
                }
            });
        },

        get_room_widgets: function (data, callback) {
            if (data.id) {
                db.collection('widget_device').find({room_id: parseInt(data.id)}, function(err, widgets) {
                    if (typeof callback == 'function') {
                        callback(widgets);
                    }
                });
            } else {
                if (typeof callback == 'function') {
                    callback([]);
                }
            }
        },

        get_dashboard_widgets: function (data, callback) {
            db.collection('widget_device').find({dashboard: 1}, function(err, widgets) {
                if (typeof callback == 'function') {
                    callback(widgets);
                }
            });
        },

        get_device_info: function (data, callback) {
            if (data.id) {
                db.collection('devices').findOne({id: parseInt(data.id)}, function (err, doc) {
                    if (typeof callback == 'function') {
                        callback(doc);
                    }
                });
            } else {
                if (typeof callback == 'function') {
                    callback(false);
                }
            }
        },

        get_device_data: function (data, callback) {
            if (data.id) {
                db.collection('devices').findOne({id: parseInt(data.id)}, function (err, doc) {
                    if (typeof callback == 'function') {
                        callback(doc);
                    }
                });
            } else {
                if (typeof callback == 'function') {
                    callback(false);
                }
            }
        },

        get_widget_data: function (data, callback) {
            if (data.id) {
                db.collection('widgets').findOne({id: parseInt(data.id)}, function (err, doc) {
                    if (typeof callback == 'function') {
                        callback(doc);
                    }
                });
            } else {
                if (typeof callback == 'function') {
                    callback(false);
                }
            }
        },

        get_all_scenarios: function (data, callback) {
            db.collection('scenarios').find({}, function(err, doc) {
                if (typeof callback == 'function') {
                    callback(doc);
                }
            });
        },

        get_all_settings: function (data, callback) {
            db.collection('settings').find({}, function(err, doc) {
                if (typeof callback == 'function') {
                    callback(doc);
                }
            });
        },

        get_all_widgets: function (data, callback) {
            db.collection('widgets').find({}, function(err, doc) {
                if (typeof callback == 'function') {
                    callback(doc);
                }
            });
        },

        set_widget_device: function (data, callback) {
            data = {
                id: parseInt(data.id),
                device_id: parseInt(data.device_id),
                widget_id: parseInt(data.widget_id),
                room_id: parseInt(data.room_id),
                dashboard: parseInt(data.dashboard),
            }
            if (data.id) {
                db.collection('widget_device').update({id: parseInt(data.id)}, {$set: data}, function (err, doc) {
                    db.collection('widget_device').findOne({id: parseInt(data.id)}, function(err, doc) {
                        if (typeof callback == 'function') {
                            callback(doc);
                        }

                        socket.broadcast.emit('change_widget_device_data', doc);
                    });
                });
            } else {
                db.collection('widget_device').find({}, function(err, doc) {
                    data.id = doc[doc.length-1].id + 1;
                    db.collection('widget_device').insert(data, function (err, doc) {
                        if (typeof callback == 'function') {
                            callback(doc[0]);
                        }

                        socket.broadcast.emit('new_widget_device_data', doc);
                    });
                });
            }
        },

        get_widget_device: function (data, callback) {
            if (data.id || data.device_id) {
                find_data = data.id ? {id: parseInt(data.id)} : {device_id: parseInt(data.device_id)};
                db.collection('widget_device').findOne(find_data, function(err, doc) {
                    var result = doc;
                    var endQuery = function () {
                        if ((result.widget || !result.widget_id)
                            && (result.device || !result.device_id)
                            && (result.room || !result.room_id)
                            && typeof callback == 'function'
                        ) {
                            callback(result);
                        }
                    }

                    if (result) {
                        if (result.widget_id) {
                            db.collection('widgets').findOne({id: parseInt(result.widget_id)}, function(err, doc) {
                                result.widget = doc;
                                delete result.widget_id

                                endQuery();
                            });
                        }

                        if (result.device_id) {
                            db.collection('devices').findOne({id: parseInt(result.device_id)}, function(err, doc) {
                                result.device = doc;
                                delete result.device_id

                                endQuery();
                            });
                        }

                        if (result.room_id) {
                            db.collection('rooms').findOne({id: parseInt(result.room_id)}, function(err, doc) {
                                result.room = doc;
                                delete result.room_id

                                endQuery();
                            });
                        }
                    } else {
                        if (typeof callback == 'function') {
                            callback(false);
                        }
                    }
                });
            }
        },

        set_device_data: function (data, callback) {
            var _this = this;

            if (data.id) {
//                db.collection('devices').update({id: parseInt(data.id)}, {$set: {state: data.state}}, function (err, doc) {
                    db.collection('devices').findOne({id: parseInt(data.id)}, function (err, doc) {
                        if (typeof callback == 'function') {
                            callback(doc);
                        }

                        socket.emit('change_device_data', doc);
                        socket.broadcast.emit('change_device_data', doc);
                        _this.on_change_device_data(doc);

                        doc.state = data.state;
                        socket.broadcast.emit('change_raw_device_data', doc);
                    });
//                });
            } else {
                if (typeof callback == 'function') {
                    callback(false);
                }
            }
        },

        get_room_data: function (data, callback) {
            if (data.id) {
                db.collection('rooms').findOne({id: data.id}, function(err, room) {
                    if (typeof callback == 'function') {
                        callback(room);
                    }
                });
            } else {
                if (typeof callback == 'function') {
                    callback(false);
                }
            }
        },

        on_change_device_data: function (data) {
            this.scenarios_search('on_change_device_data', 'device', data);
        },

        on_add_device_data: function (data) {
            this.scenarios_search('on_add_device_data', 'device', data);
        },

        on_remove_device: function (data) {
            this.scenarios_search('on_remove_device', 'device', data);
        },

        on_time: function () {
            var time = new Date();
            var data = {
                id: 0,
                name: 'time',
                state: parseInt(time.getTime() / 1000),
            };
            this.scenarios_search('on_time', 'device', data);
        },

        scenarios_search: function (event, object, data) {
            var _this = this;

            db.collection('scenarios').find({event: event}, function(err, scenariosByEvent) {
                scenariosByEvent.forEach(function(scenario) {
                    if (scenario.object.name == object && scenario.object.id == data.id) {
                        _this.scenario_init(scenario, data);
                    }
                });
            });
        },

        scenario_init: function (scenario, data) {
            var _this = this;

            for (i in scenario.value) {
                switch (i.substring(0, 2)) {
                    case '==':
                        var x = parseInt(i.substring(2, i.length));
                        if (data.state == x) {
                            _this.scenario_do(scenario.value[i], data);
                        }
                        break;
                    case '!=':
                        var x = parseInt(i.substring(2, i.length));
                        if (data.state != x) {
                            _this.scenario_do(scenario.value[i], data);
                        }
                        break;
                    case '>>':
                        var x = parseInt(i.substring(2, i.length));
                        if (data.state > x) {
                            _this.scenario_do(scenario.value[i], data);
                        }
                        break;
                    case '>=':
                        var x = parseInt(i.substring(2, i.length));
                        if (data.state >= x) {
                            _this.scenario_do(scenario.value[i], data);
                        }
                        break;
                    case '<<':
                        var x = parseInt(i.substring(2, i.length));
                        if (data.state < x) {
                            _this.scenario_do(scenario.value[i], data);
                        }
                        break;
                    case '<=':
                        var x = parseInt(i.substring(2, i.length));
                        if (data.state <= x) {
                            _this.scenario_do(scenario.value[i], data);
                        }
                        break;
                    case '<>':
                        var xn = i.substring(2, i.length).split('|');
                        if (xn.length > 1 && data.state > xn[0] && data.state < xn[1]) {
                            _this.scenario_do(scenario.value[i], data);
                        }
                        break;
                }
            }
        },

        scenario_do: function (scenario_do, data) {
            var _this = this;

            var action_set = function (object) {
                switch (object.name) {
                    case "device":
                        _this.set_device_data(object);
                        break;
                }
            }

            switch (scenario_do.action) {
                case "set":
                    scenario_do.object.forEach(function(object) {
                        console.log(object);
                        action_set(object);
                    });
                    break;
            }
        }
    }
}
