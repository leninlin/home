exec = require('child_process').exec;
i2c = require('i2c');
io = require('socket.io-client');

addressesInfo = {};
addresses = [];
busy = {};

intervals = {};
addEventInterval = function(id, func, interval) {
    if (intervals[id]) {
        clearInterval(intervals[id]);
    }
    intervals[id] = setInterval(func, interval);
}
removeEventInterval = function(id) {
    if (intervals[id]) {
        clearInterval(intervals[id]);
    }
}

socket = io.connect('http://192.168.0.18:8001');

socketConnect = function () {
    socket.socket.connect();
}

addEventInterval('socketConnect', socketConnect, 5000);

socket.on('connect', function() {
    removeEventInterval('socketConnect');
    addEventInterval('scan', scanAddresses, 50);

    socket.on('change_raw_device_data', function(device) {
        setDeviceState(device.address, device.device_num, [device.state]);
    });

    socket.on('disconnect', function() {
        removeEventInterval('scan');
        addEventInterval('socketConnect', socketConnect, 5000);
    });
});

scanAddresses = function () {
    scanI2C(function(addresses, add_addresses, remove_addresses) {
        if (remove_addresses.length) {
            socket.emit('remove_addresses', {addresses: remove_addresses});
            remove_addresses.forEach(function(address) {
                delete addressesInfo[address];
            });
        }

        if (add_addresses.length) {
            add_addresses.forEach(function(address) {
                console.log('Find new address', address);
                getAddressInfo(address, function(info) {
                    addressesInfo[address] = info;
                    socket.emit('add_address', {address: address, devices: info.devices});
                });
            });
        }

        if (addresses.length) {
            addresses.forEach(function(address) {
                if (addressesInfo[address] && !busy.address) {
                    for (i in addressesInfo[address].devices) {
                        getDeviceState(addressesInfo[address].conn, parseInt(i), function(state) {
                            socket.emit('set_device_raw_state', {address: address, device_num: i, state: state});
                            addressesInfo[address].devices[i].state = state;
                        });
                    }
                }
            });
        }
    });
};

scanI2C = function(callback) {
    exec('i2cdetect -y 1', function(err, stdout, stderr) {
        var scanedAddresses = [];
        var lines = stdout.split('\n');
        lines.shift();
        for (i in lines) {
            var line = lines[i].split(' ');
            line.shift();
            for (k in line) {
                if (line[k] != '' && line[k] != '--') {
                    scanedAddresses.push(parseInt(line[k], 16));
                }
            }
        }

        var diff = array_diff(addresses, scanedAddresses);
        addresses = scanedAddresses;

        if (typeof callback == 'function') {
            callback(addresses, diff.add, diff.remove);
        }
    });
}

getAddressInfo = function(address, callback) {
    console.log('Get info from new address', address);
    var info = {arrdess: address};
    var countGetDevices = 0;

    info.conn = new i2c(address, {device: '/dev/i2c-1'});

    var send_callback = function() {
        if (countGetDevices == info.countDevices) {
            console.log('Geted info', info);
            if (typeof callback == 'function') {
                callback(info);
            }
        }
    }

    // get count devices on address
    info.conn.readBytes(1, 1, function(err, res) {
        console.log('Get count devices', res);
        if (parseInt(res[0], 16) > 0) {
            info.countDevices = parseInt(res[0]);

            info.devices = info.devices || {};

            for (i = 0; i < info.countDevices; i++) {
                info.devices[i] = info.devices[i] || {};
                getDeviceType(info.conn, i, function(typeSensor, typeAction) {
                    info.devices[i].sensor = typeSensor;
                    info.devices[i].action = typeAction;
                    getDeviceName(info.conn, i, function(name) {
                        info.devices[i].name = name;
                        getDeviceState(info.conn, parseInt(i), function(state) {
                            info.devices[i].state = state;
                            countGetDevices++;
                            send_callback();
                        });
                    });
                });
            }
        }
    });
}

getDeviceType = function(conn, device, callback) {
    if (conn) {
        conn.writeBytes(2, [device], function(err) {
            conn.readBytes(2, 1, function(err, res) {
                console.log('Get type from device', device, ' : ', res, ' / ', err);
                var deviceSensor = getDeviceSensorType(parseInt(res[0]));
                var deviceAction = getDeviceActionType(parseInt(res[0]));
                if (typeof callback == 'function') {
                    callback(deviceSensor, deviceAction);
                }
            });
        });
    }
}

getDeviceName = function(conn, device, callback) {
    if (conn) {
        conn.writeBytes(3, [device], function(err) {
            conn.readBytes(3, 20, function(err, res) {
                console.log('Get name from device', device, ' : ', res, ' / ', err);
                if (!err) {
                    var name = '';
                    for (k = 0; k < 20; k++) {
                        if (res[k] != 0xff && res[k] != 0x00) {
                            name += String.fromCharCode(res[k]);
                        }
                    }

                    if (typeof callback == 'function') {
                        callback(name);
                    }
                }
            });
        });
    }
}

getDeviceState = function(conn, device, callback) {
    if (conn) {
        conn.writeBytes(4, [device], function(err) {
            conn.readBytes(4, 4, function(err, res) {
                console.log('Get state from device', device, ' : ', res, ' / ', err);
                if (!err) {
                    if (typeof callback == 'function') {
                        callback(parseInt(res[0]));
                    }
                }
            });
        });
    }
}

setDeviceState = function(address, device, data) {
    if (addressesInfo[address] && addressesInfo[address].conn) {
        busy.address = true;
        data.unshift(device);
        addressesInfo[address].conn.writeBytes(5, data, function(err) {
            console.log('Set state to device', device, ' : ', data, ' / ', err);
            setTimeout(function() {busy.address = false;}, 100);
        });
    }
}

array_diff = function(arr1, arr2) {
    var arr_diff = function(arr1, arr2) {
        var diff = [];
        arr1.forEach(function(item1) {
            var exist = false;
            arr2.forEach(function(item2) {
                if (item1 == item2) {
                    exist = true;
                }
            });

            if (!exist) {
                diff.push(item1);
            }
        });

        return diff;
    }

    return {
        add: arr_diff(arr2, arr1),
        remove: arr_diff(arr1, arr2),
    };
}

getDeviceSensorType = function(type) {
    switch (type) {
        case 2:
        case 5:
            return false;

        case 1:
        case 3:
            return {
                type: 'bool',
            };

        case 4:
        case 6:
            return {
                type: 'int',
            };
        case 7:
            return {
                type: 'bool',
            };
    }
}

getDeviceActionType = function(type) {
    switch (type) {
        case 1:
        case 4:
            return false;

        case 2:
        case 3:
            return {
                type: 'bool',
            };

        case 5:
        case 6:
            return {
                type: 'int',
            };
        case 7:
            return {
                type: 'array',
            };
    }
}